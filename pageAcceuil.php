<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Test PDO</title>
  </head>
  <body>
    <?php
      require('./modele.php');
      require_once('connexion.php');
      $dsn="mysql:dbname=".BASE.";host=".SERVER;
      $connexion=new PDO($dsn,USER,PASSWD);
    ?>
  </body>
</html>
