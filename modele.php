
<?php
require_once('./connexion.php');

function movieByName($filmName){
  if (!empty($filmName)){
    $maj= ucfirst($filmName);
    $dsn="mysql:dbname=".BASE.";host=".SERVER;
    $connexion=new PDO($dsn,USER,PASSWD);
    $sql="SELECT *
          from FILM
          where titreOriginalF LIKE '%$filmName%' or titreOriginalF LIKE '%$maj%'";
    $stmt=$connexion->prepare($sql);
    $stmt->execute();
    return $sql;
    }
  }

function getIdRealByName($realName){
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
  $connexion=new PDO($dsn,USER,PASSWD);
  $sql="SELECT distinct idP
        from PERSONNE
        where nomP='$realName'";
  $stmt=$connexion->prepare($sql);
  $stmt->execute();
  return $sql;
}

function deleteFilm($filmId){
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
  $connexion=new PDO($dsn,USER,PASSWD);
  $sql="DELETE
        from FILM
        where idF='$filmId'";
  $stmt=$connexion->prepare($sql);
  $stmt->execute();
  return $sql;
}

function getMaxId(){
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
  $connexion=new PDO($dsn,USER,PASSWD);
  $sql="SELECT max(idF)
        from FILM";
  $stmt=$connexion->prepare($sql);
  $stmt->execute();
  return $sql;
}

function addFilm($filmId, $titre, $titreFr, $pays, $date, $duree, $couleur, $realisateur){
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
  $connexion=new PDO($dsn,USER,PASSWD);
  $sql="INSERT INTO FILM(idF, titreOriginalF, titreFrancaisF, paysF,
                      dateF, dureeF, couleurF, realisateurF, imageF)
        VALUES (:idF, :titreO, :titreF, :paysF,
                      :dateF, :dureeF, :colF, :realisateurF, :imageF)";
  $stmt=$connexion->prepare($sql);
  $img='default.jpg';
  $stmt->bindParam(':idF',$filmId);
  $stmt->bindParam(':titreO',$titre);
  $stmt->bindParam(':titreF',$titreFr);
  $stmt->bindParam(':paysF',$pays);
  $stmt->bindParam(':dateF',$date);
  $stmt->bindParam(':dureeF',$duree);
  $stmt->bindParam(':colF',$couleur);
  $stmt->bindParam(':realisateurF',$realisateur);
  $stmt->bindParam(':imageF',$img);
  $stmt->execute();
}

function modifFilm($filmId, $titre, $titreFr, $pays, $date, $duree, $couleur, $realisateur){
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
  $connexion=new PDO($dsn,USER,PASSWD);
  $img='default.jpg';
  $sql="UPDATE FILM SET titreOriginalF='$titre', titreFrancaisF='$titreFr',
        paysF='$pays', dateF='$date', dureeF='$duree', couleurF='$couleur',
        realisateurF='$realisateur', imageF='$img' where idF='$filmId'";
  $conn = new mysqli("servinfo-mariadb", "nouaille", "nouaille", "DBnouaille");
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
  if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully";
  } else {
      echo "Error updating record: " . $conn->error;
  }


}

function getListRealisateur(){
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
  $connexion=new PDO($dsn,USER,PASSWD);
  $sql="SELECT *
        from PERSONNE";
  $stmt=$connexion->prepare($sql);
  $stmt->execute();
  return $sql;
}

function getListGenre(){
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
  $connexion=new PDO($dsn,USER,PASSWD);
  $sql="SELECT *
        from GENRE";
  $stmt=$connexion->prepare($sql);
  $stmt->execute();
  return $sql;
}

function movieById($filmId){
  if (!empty($filmId)){
    $dsn="mysql:dbname=".BASE.";host=".SERVER;
    $connexion=new PDO($dsn,USER,PASSWD);
    $sql="SELECT *
          from FILM
          where idF='$filmId'";
    $stmt=$connexion->prepare($sql);
    $stmt->execute();
    return $sql;
    }
  }

function movieByGenre($filmGenre){
  if (!empty($filmGenre)){
    $maj= ucfirst($filmGenre);
    $dsn="mysql:dbname=".BASE.";host=".SERVER;
    $connexion=new PDO($dsn,USER,PASSWD);
    $sql="SELECT distinct titreOriginalF, nomG
          from CLASSIFICATION natural join GENRE natural join FILM
          where nomG LIKE '%$filmGenre%' or nomG LIKE '%$maj%'";
    $stmt=$connexion->prepare($sql);
    $stmt->execute();
    return $sql;
  }
}
?>
