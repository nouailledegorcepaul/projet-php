<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="header.css"/>
    <title></title>
  </head>
  <body>
<div class="header">
  <a id="title" href="index.php">FilmsActu</a>
  <a id="ajouter" class="btn btn-outline-success" aria-pressed="true" href="./AjouterFilm.php" >Ajouter  <i class="fa fa-plus" aria-hidden="true"></i></a>
  <div class="search-container">
    <form action="recherche.php" method="get">
      <input id="text" type="text" placeholder="Search.." name="search">
      <button id="search" type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>
</div >
</body>
</html>
