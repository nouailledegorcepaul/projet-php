<html>
  <head>
    <title>
      Recherche
    </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="tabstyle.css" />
    <link rel="stylesheet" href="recherche.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <?php
    require("header.php");
    require("modele.php");
    require_once('connexion.php');
    ?>
    <?php

    $connexion=connect_bd();

    if(!empty($_POST['Supprimer'])) {
        deleteFilm($_POST['Supprimer']);
    }
    if(!empty($_POST['Modifier'])) {
        modifFilm($_POST['Supprimer']);
    }
    if (isset($_GET['search']))
    {
      $sql=movieByName($_GET['search']);
      if(!$connexion->query($sql)) echo "Pb d'accès aux Films";
      else{
        echo "<div class='row'>";
        echo "<div class='col-md-2'></div>";
        echo "<h1 class='col-md-8 col-12'>Résultat pour ".$_GET['search']."</h1>";
        echo "<div class='col-md-2'></div>";
        echo "<div class='col-md-2'></div>";
        echo "<ul class='list-group col-md-8 col-12 '>";

        $cpt=0;
        foreach ($connexion->query($sql) as $row){
          $idFilm = $row['idF'];
          $motrecherche = $_GET['search'];
          if($row['dateF']>2018){
            echo
                "<li class='list-group-item row'><a class='col-10' data-toggle='collapse' href='#collapse".$cpt."'>".$row['titreOriginalF']."  "."<span class='badge badge-secondary'>New</span></a>
                    <form action='AjouterFilm.php?id=$idFilm' method='get'>
                      <input class='btn btn-success' type='submit' id='Modifier'.$idFilm name='id' value='$idFilm'>
                    </form>
                    <form action='recherche.php?search=$motrecherche' method='post'>
                      <input class='btn btn-danger' type='submit' id='Supprimer'.$idFilm name='Supprimer' value='$idFilm'>
                    </form>
                 </li>";
          }
          else{
            echo "<li class='list-group-item row'><a class='col-10' data-toggle='collapse' href='#collapse".$cpt."'>".$row['titreOriginalF']."</a>
                    <form action='AjouterFilm.php?id=$idFilm' method='get'>
                      <input class='btn btn-success' type='submit' id='Modifier'.$idFilm name='id' value='$idFilm'>
                    </form>
                    <form action='recherche.php?search=$motrecherche' method='post'>
                      <input class='btn btn-danger' type='submit' id='Supprimer'.$idFilm name='Supprimer' value='$idFilm'>
                    </form>
                 </li>";
          }

          echo "<div id='collapse".$cpt."' class='panel-collapse collapse col-md-8 col-12'>
                  <div class='panel-body'>Titre français : ".$row['titreFrancaisF']."
                  \n<br/>Réalisateur : ".$row['realisateurF']."
                  \n<br/>Pays : ".$row['paysF']."
                  \n<br/>Date de sortie : ".$row['dateF']."
                  \n<br/>Durée du film : ".$row['dureeF']." min
                  \n<br/>Film en ".$row['couleurF']."

                  </div>

                </div>";
          $cpt=$cpt+1;
        }
        echo "</ul></div>";
      }
    }
    else if (isset($_GET['genre'])){
      $sql=movieByGenre($_GET['genre']);
      if(!$connexion->query($sql)) echo "Pb d'accès aux Films";
      else{
        foreach ($connexion->query($sql) as $row){
          echo $row['titreOriginalF']."<br/>";
        }
        echo "Fin de recherche";
      }
    }
    else if (isset($_GET['id'])){
      $sql=movieById($_GET['id']);
      if(!$connexion->query($sql)) echo "Pb d'accès aux Films";
      else{
        foreach ($connexion->query($sql) as $row){
          echo $row['titreOriginalF']."<br/>";
        }
        echo "Fin de recherche";
      }
    }
    else{
      echo "Pas de recherche";
    }
    ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
  </html>
