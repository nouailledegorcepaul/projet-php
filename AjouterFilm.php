<html>
  <head>
    <title>
      Ajouter Film
    </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="tabstyle.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <?php
      require("header.php");
      require("modele.php");
    ?>

    <?php
      require_once("connexion.php");
      $connexion=connect_bd();
      $idRealisateur=null;
      if (isset($_POST['realisateurF'])){
        $realName=$_POST['realisateurF'];
        $realId = getIdRealByName($realName);
        if(!$connexion->query($realId)) echo "Pb d'accès à la BD realisateurF";
        else{
          foreach ($connexion->query($realId) as $row){
            $idRealisateur=$row['idP'];
          }
        }
      }
      if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_GET['id'])) {
         modifFilm($_GET['id'],$_POST['titre'], $_POST['titreF'], $_POST['pays'],
          $_POST['date'], $_POST['duree'], $_POST['optradio'], $idRealisateur);
      }
      else if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id=getMaxId();
        foreach ($connexion->query($id) as $row){
          $idmax=$row['max(idF)']+1;
          addFilm($idmax,$_POST['titre'], $_POST['titreF'], $_POST['pays'],
          $_POST['date'], $_POST['duree'], $_POST['optradio'], intval($idRealisateur));
        }
      }
    ?>
    <div class="col-12 row" style="margin-top:50px;">
      <div class="col-2"></div>




      <?php
      if(!isset($_GET['id'])){

       ?>
       <form class="form-inline jumbotron col-8" method="POST" action="AjouterFilm.php">
      <div class="col-12 row">
        <label for="Titre" class="mr-sm-2 col-3">Titre :</label>
        <input type="text" class="form-control mb-2 mr-sm-2 col-8" name="titre" id="titre" required>
        <label for="TitreF" class="mr-sm-2 col-3">Titre français :</label>
        <input type="text" class="form-control mb-2 mr-sm-2 col-8" name="titreF" id="titreF" required>
      </div>
      <div class="col-12 row">
        <label for="Pays" class="mr-sm-2 col-3">Pays :</label>
        <input type="text" class="form-control mb-2 mr-sm-2 col-8" name="pays" id="pays">
        <label for="Date" class="mr-sm-2 col-3">Date :</label>
        <input type="date" class="form-control mb-2 mr-sm-2 col-4" name="date" id="datex">
      </div>
      <label for="Durée" class="mr-sm-2 col-3">Durée :</label>
      <input type="number" class="form-control mb-2 mr-sm-2 col-2" name="duree" id="duree" required>
      <label for="Durée" class="mr-sm-2 col-2">Couleur :</label>

            <label class="radio-inline"><input type="radio" name="optradio" value='couleur' checked>Oui</label>
            <label class="radio-inline"><input type="radio" name="optradio" value='NB'>Non</label>

      <div class="col-12 row"$idFilm>
        <label for="Real" class="mr-sm-2 col-3">Réalisateur :</label>

        <select name="realisateurF" class="col-5">
          <?php

            require_once('connexion.php');

            $connexion=connect_bd();
            $reali = getListRealisateur();
            if(!$connexion->query($reali)) echo "Pb d'accès à la BD";
            else{
              foreach ($connexion->query($reali) as $row){
                echo "<option value=".$row['nomP'].">".$row['nomP']."</option>";
              }
            }
          ?>
       </select>
     </div>



     <div class="col-12 row">
       <label for="genre" class="mr-sm-2 col-3">Genre :</label>

     <select name="genre" class="col-5">
       <?php

         $connexion=connect_bd();
         $genre = getListGenre();
         if(!$connexion->query($genre)) echo "Pb d'accès à la BD";
         else{
           foreach ($connexion->query($genre) as $row){
             echo "<option value=".$row['nomG'].">".$row['nomG']."</option>";
           }
         }
       ?>
     </select></div>
     <div class="col-12"></div>
      <button type="submit" class="btn btn-primary mb-2 col-12">Ajouter le film</button>

   <?php
  }
  else{
    $sqlIdF = movieById($_GET['id']);
    $titreO=null;
    $titreF=null;
    $paysF=null;
    $dateF=null;
    $dureeF=null;
    $couleur=null;
    $realisateur=null;
    foreach ($connexion->query($sqlIdF) as $row){
      $titreO=$row['titreOriginalF'];
      $titreF=$row['titreFrancaisF'];
      $paysF=$row['paysF'];
      $dateF=$row['dateF'];
      $dureeF=$row['dureeF'];
      $couleur=$row['couleurF'];
      $realisateur=$row['realisateurF'];
    }
    $id=$_GET['id'];
    echo "<form class='form-inline jumbotron col-8' method='POST' action='AjouterFilm.php?id=$id'>";
    ?>

   <div class="col-12 row">
     <label for="Titre" class="mr-sm-2 col-3">Titre :</label>
     <?php
     echo "<input type='text' class='form-control mb-2 mr-sm-2 col-8' name='titre' value='$titreO' id='titre' required>"; ?>

     <label for="TitreF" class="mr-sm-2 col-3">Titre français :</label>
     <?php
     echo "<input type='text' class='form-control mb-2 mr-sm-2 col-8' value='$titreF' name='titreF' id='titreF' required>";
     ?>
   </div>
   <div class="col-12 row">
     <label for="Pays" class="mr-sm-2 col-3">Pays :</label>
     <?php
     echo "<input type='text' class='form-control mb-2 mr-sm-2 col-8' value='$paysF' name='pays' id='pays'>";
     ?>
     <label for="Date" class="mr-sm-2 col-3">Année :</label>
     <?php
     echo "<input type='number' value='$dateF' class='form-control mb-2 mr-sm-2 col-4' name='date' id='datex'>";
     ?>
   </div>
   <label for="Durée" class="mr-sm-2 col-3">Durée :</label>
   <?php
   echo "<input type='number' value='$dureeF' class='form-control mb-2 mr-sm-2 col-2' name='duree' id='duree' required>";
   ?>
   <label for="Durée" class="mr-sm-2 col-2">Couleur :</label>
        <?php
        if($couleur=="NB"){
          echo "<label class='radio-inline'><input type='radio' name='optradio' value='couleur'>Oui</label>
          <label class='radio-inline'><input type='radio' name='optradio' checked value='NB'>Non</label>";
        }else{
          echo "<label class='radio-inline'><input type='radio' name='optradio' value='couleur' checked>Oui</label>
          <label class='radio-inline'><input type='radio' name='optradio'  value='NB'>Non</label>";
        }
         ?>

   <div class="col-12 row">
     <label for="Real" class="mr-sm-2 col-3">Réalisateur :</label>

     <select name="realisateurF" class="col-5">
       <?php

         require_once('connexion.php');

         $connexion=connect_bd();
         $reali = getListRealisateur();
         if(!$connexion->query($reali)) echo "Pb d'accès à la BD";
         else{
           foreach ($connexion->query($reali) as $row){

             echo "<option value=".$row['nomP'].">".$row['nomP']."</option>";
           }
         }
       ?>
    </select>
  </div>



  <div class="col-12 row">
    <label for="genre" class="mr-sm-2 col-3">Genre :</label>

  <select name="genre" class="col-5">
    <?php

      $connexion=connect_bd();
      $genre = getListGenre();
      if(!$connexion->query($genre)) echo "Pb d'accès à la BD";
      else{
        foreach ($connexion->query($genre) as $row){
          echo "<option value=".$row['nomG'].">".$row['nomG']."</option>";
        }
      }
    ?>
  </select></div>
  <div class="col-12"></div>
   <button type="submit" class="btn btn-success mb-2 col-12">Modifier le film</button>

 <?php }?>


    </form>      <div class="col-2"> </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
